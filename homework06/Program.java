/*
Реализовать функцию, принимающую на вход массив и целое число. 
Данная функция должна вернуть индекс этого числа в массиве. 
Если число в массиве отсутствует - вернуть -1.

Реализовать процедуру, которая переместит 
все значимые элементы влево, заполнив нулевые, например:

было:
34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20

стало
34, 14, 15, 18, 1, 20, 0, 0, 0, 0, 0

*/


import java.util.Scanner;

class Program {
	// функция вывода индекса элемента массива  
	public static int getArrayElementIndex (int[] array, int number) {

		for (int i = 0; i < array.length; i++) {
			if (array[i] == number) {
				return i;
			}
		}

		return -1;
	}

	// процедура принимает массив int и сдвигает значимые элементы влево
	public static void moveValidArrayElementsLeft (int[] arrayRef) {
		int[] array = arrayRef;
		// перебираем элементы по порядку
		for (int i = 0; i < array.length; i++) {

			// ищем нулевой элемент
			if (array[i] == 0) {
				for (int j = i+1; j < array.length; j++) {
					// от i-го элемента ищем следующий ненулевой элемент
					if (array[j] != 0) {

						// заменяем текущий на ненулевой
						array[i] = array[j];
						// обнуляем ненулевой
						array[j] = 0;

						// выходим из цикла
						break;
					}
				}
			} 
		}

	}



	// для проверки - вывод элементов массива в строку
	public static void printArray (int[] array) {
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
	 	System.out.println();
	}




	public static void main(String[] args) {
		int[] arr = {3, 10, -5, 15, 23, 48, -30};
		int[] arr1 = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};

		printArray(arr);
		System.out.println(getArrayElementIndex(arr, 15));
		printArray(arr1);
		moveValidArrayElementsLeft(arr1);
		printArray(arr1);
	}


}