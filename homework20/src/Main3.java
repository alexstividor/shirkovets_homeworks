import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;


// * Вывести цвет автомобиля с минимальной стоимостью. // min + map
public class Main3 {
    public static void main(String[] args) {
        // резервируем переменную под массив слов отобранной строки считанной из файла
        String[] words;
        try (BufferedReader reader = new BufferedReader(new FileReader("homework20/input.txt"))) {
            //собираем стрим массивов строк из прочитанного
            words = reader.lines()
                    .distinct()
                    .map(line -> line.split("\\|"))
                    .min(Comparator.comparing(line -> Integer.parseInt(line[4])))
                    .get();

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        // System.out.println("цвет - " + words[2] + " ..... для проверки: "+  " стоимость - " + words[4] );
        // вывожу в поток вывода с помощью метода String.format:
        System.out.println(String.format("цвет - %s ..... для проверки: стоимость - %s", words[2], words[4]));
    }
}
