import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/*
Подготовить файл с записями, имеющими следующую структуру:
[НОМЕР_АВТОМОБИЛЯ][МОДЕЛЬ][ЦВЕТ][ПРОБЕГ][СТОИМОСТЬ]

o001aa111|Camry|Black|133|82000
o002aa111|Camry|Green|133|0
o001aa111|Camry|Black|133|82000

Используя Java Stream API, вывести:

Номера всех автомобилей, имеющих черный цвет или нулевой пробег. // filter + map
Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс. // distinct + filter
* Вывести цвет автомобиля с минимальной стоимостью. // min + map
* Среднюю стоимость Camry *

https://habr.com/ru/company/luxoft/blog/270383/
Достаточно сделать один из вариантов.
 */

// Номера всех автомобилей, имеющих черный цвет или нулевой пробег. // filter + map
public class Main1 {
    public static void main(String[] args) {


        try (BufferedReader reader = new BufferedReader(new FileReader("homework20/input.txt"))) {
            //собираем стрим строк из прочитанного
            reader.lines()
                    // выбираем уникальные строки
                    .distinct()
                    // разбираем строки на слова
                    .map(line -> line.split("\\|"))
                    // фильтруем по условию черного цвета или нулевого пробега
                    .filter(string -> (string[2].equals("Black") || string[3].equals("0")))
                    // выводим результат
                    .forEach(st ->
                            System.out.println(String.format("номер - %s ..... для проверки: цвет - %s, пробег - %s", st[0], st[2], st[3])));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}

