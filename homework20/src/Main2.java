import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.stream.Collectors;


/*
Подготовить файл с записями, имеющими следующую структуру:
[НОМЕР_АВТОМОБИЛЯ][МОДЕЛЬ][ЦВЕТ][ПРОБЕГ][СТОИМОСТЬ]

o001aa111|Camry|Black|133|82000
o002aa111|Camry|Green|133|0
o001aa111|Camry|Black|133|82000

Используя Java Stream API, вывести:

Номера всех автомобилей, имеющих черный цвет или нулевой пробег. // filter + map
Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс. // distinct + filter
* Вывести цвет автомобиля с минимальной стоимостью. // min + map
* Среднюю стоимость Camry *

https://habr.com/ru/company/luxoft/blog/270383/
Достаточно сделать один из вариантов.
 */

// Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс. // distinct + filter
public class Main2 {
    public static void main(String[] args) {
        long countOfUnique;
        try (BufferedReader reader = new BufferedReader(new FileReader("input.txt"))) {
            countOfUnique = reader.lines()
                    // выбираем уникальные строки
                    .distinct()
                    // разбираем на массив строк
                    .map(line -> line.split("\\|"))
                    // фильтруем по цене - string[4]
                    .filter(string -> (Integer.parseInt(string[4]) >= 700000 && Integer.parseInt(string[4]) <= 800000))
                    // выбираем в стрим названия моделей
                    .map(string -> string[1])
                    // выбираем уникальные названия моделей
                    .distinct()
                    // забираем количество уникальных названия моделей
                    .count();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        System.out.println(countOfUnique);
    }
}
