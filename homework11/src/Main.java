public class Main {

    /*
    Сделать класс Logger с методом void log(String message), который выводит в консоль какое-либо сообщение.
    Применить паттерн Singleton для Logger.
    */

    public static void main(String[] args) {
        Logger logger = Logger.getInstance();

        logger.add("message 01");
        logger.add("message 02");
        logger.add("message 03");

        logger.log();

        logger.log("message 04");
    }
}
