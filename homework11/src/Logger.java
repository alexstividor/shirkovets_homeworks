public class Logger {
    // создаем статическое поле хранящее единственный экземпляр класса
    private static final Logger instance;

    //создаем статический инициализатор создающий единственный экземпляр класса
    static {
        instance = new Logger();
    }

    //создаем статический метод для получения единственного экземпляра объекта
    public static Logger getInstance(){
        return instance;
    }

    private static final int MAX_LOGS_COUNT = 100;
    private String logs[];
    private int count;

    private Logger() {
        this.logs = new String[MAX_LOGS_COUNT];
    }

    // метод, который сохраняет сообщения лога
    public void add(String message) {
        if (count < MAX_LOGS_COUNT) {
            this.logs[count] = message;
            count ++;
        } else {
            System.err.println("Переполнение лога");
        }
    }

    // метод, который выводит в консоль какое-либо сообщение лога
    public void log (String message) {
            System.out.println(message);
    }

    // метод, который выводит в консоль все сообщения лога
    public void log () {
        for (int i = 0; i < count; i++) {
            System.out.println(logs[i]);
        }
    }
}
