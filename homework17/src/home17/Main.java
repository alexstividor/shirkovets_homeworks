package src.home17;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        // создаем проверчный массив
        String string = ("1 1 2 2 3 3 3 4 4 4 4 4");

        // с помощью метода  split создаем массив String cо словами разделенными " "
        String[] strings = string.split(" ");

        // создаем новый объект HashMap
        Map<String, Integer> map = new HashMap<>();

        // проходим по массиву слов
        for (String s : strings) {

            // инкрементируем значение value у map, если приходит повторяющееся слово
            if (map.containsKey(s)) {
                map.put(s, map.get(s) + 1);
            } else {
                // если слово новое присваеваем value 1
                map.put(s, 1);
            }
        }

        // собираем множество (ключ, значение)
        Set<Map.Entry<String, Integer>> entries = map.entrySet();

        // выводим на консоль
        for (Map.Entry<String, Integer> entry : entries) {
            System.out.println( new StringBuilder().append("Слово - /").append(entry.getKey())
                    .append("/ количество раз - ").append(entry.getValue())
                    .toString());
        }
    }

}

/*
    На вход подается строка с текстом. Необходимо посчитать, сколько встречается раз каждое слово в этой строке.

        Вывести:
        Слово - количество раз

        Использовать Map, string.split(" ") - для деления текста по словам. Слово - символы, ограниченные пробелами справа и слева.
*/