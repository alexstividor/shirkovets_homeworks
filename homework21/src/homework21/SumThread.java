package homework21;

import static homework21.Main.array;

public class SumThread extends Thread{
    private int from;
    private int to;
    private int sum = 0;

    public SumThread(int from, int to) {
        this.from = from;
        this.to = to;
    }

    @Override
    public void run() {
        for (int i = from; i <= to; i++) {
            sum += array[i];
        }
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }
}
