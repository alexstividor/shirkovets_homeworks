package homework21;

import java.util.Random;
import java.util.Scanner;

public class Main {
    public static int[] array;

    public static int[] sums;

    public static void main(String[] args) {
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите количество чисел:");
        int numberCount = scanner.nextInt();

        System.out.println("Введите количество потоков:");
        int threadsCount = scanner.nextInt();

        array = new int[numberCount];
        sums = new int[threadsCount];

        // заполняем случайными числами
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100);
        }

        int realSum = 0;

        for (int i = 0; i < array.length; i++) {
            realSum += array[i];
        }

        // для 2 000 000 -> 98996497, 98913187
        System.out.println("Сумма чисел без потоков - " + realSum);

        // создаем логику создания потоков

        // создаем ссылку на массив потоков
        SumThread[] threads = new SumThread[threadsCount];
        // создаем переменную начального индекса массива
        int from;
        // создаем переменную конечного индекса массива
        int to;
        // создаем переменную суммы значений элементов из потоков
        int byThreadSum = 0;
        // создаем переменную для количества индексов передаваемых в поток
        int treadLengthCount;
        // количество элементов массива передаваемых в поток
        // изменяется в зависимости от кратности количеству потоков
        if (numberCount % threadsCount == 0) {
            treadLengthCount = numberCount / threadsCount;
        } else {
            treadLengthCount = (numberCount / threadsCount) + 1;
        }


        for (int j = 0; j < threadsCount; j++) {
            //формируем индекс начала массива в потоке
            from = j * treadLengthCount;
            //формируем индекс конца массива в потоке
            if (j != threadsCount - 1) {
                to = (j + 1) * treadLengthCount - 1;
            } else to = numberCount - 1;

            //создаем потоки для рассчетов
            threads[j] = new SumThread(from, to);
            threads[j].setSum(0);
            //запускаем потоки
            threads[j].start();
        }
        // ожидаем завершения потоков
        for (int i = 0; i < threadsCount; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
               e.printStackTrace();
            }
            // суммируем искомую сумму
            byThreadSum += threads[i].getSum();
        }

        System.out.println("Сумма чисел с потоками - " + byThreadSum);

    }
}
