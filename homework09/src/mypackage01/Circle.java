package mypackage01;

public class Circle extends Ellipse{

    public Circle (int x, int y, double radius1) {
        super(x, y , radius1, radius1);
    }

    public double getPerimeter() {
        return 2 * Math.PI * radius1 ;
    }

    public void paint() {
        System.out.print("Круг с координатами: " + xCoordinate + ", " + yCoordinate);
        System.out.println(" с радиусом: " + radius1);
        System.out.println("-- Периметр круга равен " + this.getPerimeter());
    }
}
