package mypackage01;

public class Ellipse extends Figure {
    protected double radius1;
    private double radius2;

    public Ellipse(int x, int y, double radius1, double radius2) {
        super(x, y);
        this.radius1 = radius1;
        this.radius2 = radius2;
    }

    public double getPerimeter() {
        return (2 * Math.PI* Math.sqrt (((radius1 * 2 + radius2 * 2) / 2.0)));
    }

    public void paint() {
        System.out.print("Эллипс с координатами: " + xCoordinate + ", " + yCoordinate);
        System.out.println(" и радиусами: " + radius1 + ", " + radius2);
        System.out.println("-- Периметр эллипса равен " + this.getPerimeter());
    }
}
