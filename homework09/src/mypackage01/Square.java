package mypackage01;

public class Square extends Rectangle{

    public Square (int x, int y, int a) {
        super(x, y , a, a);
    }

    public double getPerimeter() {
        return 4 * sideA ;
    }

    public void paint() {
        System.out.print("Квадрат с координатами: " + xCoordinate + ", " + yCoordinate);
        System.out.println(" со стороной: " + sideA);
        System.out.println("-- Периметр квадрата равен " + this.getPerimeter());
    }
}
