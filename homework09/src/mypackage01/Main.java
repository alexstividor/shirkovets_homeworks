package mypackage01;

/*Сделать класс Figure, у данного класса есть два поля - x и y координаты.
        Классы Ellipse и Rectangle должны быть потомками класса Figure.
        Класс Square - потомок класса Rectangle, Circle - потомок класса Ellipse.
        В классе Figure предусмотреть метод getPerimeter(), который возвращает 0. Во всех остальных классах он должен возвращать корректное значение.
*/

public class Main {

    public static void main(String[] args) {
        // Создаем объекты классов с помощью конструкторов
        Figure figure = new Figure(3,4);
        Rectangle rectangle = new Rectangle(10,20, 12, 20);
        Ellipse ellipse = new Ellipse(5,13, 6.0, 12.0);
        Square square = new Square(0, 23, 17);
        Circle circle = new Circle(7,23, 10.0);

        // Создаем массив предков и присваиваем поэлементно ссылку на созданные объекты потомков
        Figure[] figures = new Figure[4];
        figures[0] = rectangle;
        figures[1] = ellipse;
        figures[2] = square;
        figures[3] = circle;

        // проверяем особенности работы метода у потомков
        figure.paint();
        rectangle.paint();
        ellipse.paint();
        square.paint();
        circle.paint();

        // Вызываем методы потомков в цикле
        for (int i = 0; i < figures.length; i++) {
            figures[i].paint();
        }

    }
}
