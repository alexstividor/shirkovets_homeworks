package mypackage01;

public interface Teleport {
    /**
     * Descriptive of moveTo
     *
     * @param x set x coordinate to move to
     * @param y set x coordinate to move to
     */
    void moveTo(int x,int y);
    /**
     * Descriptive of moveToStart
     * moves any figure to start point

     */
    void moveToStart();
}
