package mypackage01;

public class Rectangle extends Figure {
    protected int sideA;
    private int sideB;

    public Rectangle (int x, int y, int a, int b) {
        super(x, y);
        this.sideA = a;
        this.sideB = b;
    }

    public double getPerimeter() {
        return sideA * 2 + sideB * 2;
    }

    public void paint() {
        System.out.print("Прямоугольник с координатами: " + xCoordinate + ", " + yCoordinate);
        System.out.println(" со сторонами: " + sideA + ", " + sideB);
        System.out.println("-- Периметр прямоугольника равен " + this.getPerimeter());
    }

}
