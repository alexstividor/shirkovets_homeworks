package mypackage01;

public class Square extends Rectangle implements Teleport{

    public Square (int x, int y, int a) {
        super(x, y , a, a);
    }

    public double getPerimeter() {
        return 4 * sideA ;
    }

    @Override
    public void moveTo (int x, int y) {
        this.xCoordinate = x;
        this.yCoordinate = y;
    }

    @Override
    public void moveToStart () {
        this.xCoordinate = 0;
        this.yCoordinate = 0;
    }


    public void paint() {
        System.out.print("Квадрат с координатами: " + xCoordinate + ", " + yCoordinate);
        System.out.println(" со стороной: " + sideA);
        System.out.println("-- Периметр квадрата равен " + this.getPerimeter());
    }
}
