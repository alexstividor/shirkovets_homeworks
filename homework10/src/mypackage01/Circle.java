package mypackage01;

public class Circle extends Ellipse implements Teleport {

    public Circle (int x, int y, double radius1) {
        super(x, y , radius1, radius1);
    }

    public double getPerimeter() {
        return 2 * Math.PI * radius1 ;
    }

    @Override
    public void moveTo (int x, int y) {
        this.xCoordinate = x;
        this.yCoordinate = y;
    }

    @Override
    public void moveToStart () {
        this.xCoordinate = 0;
        this.yCoordinate = 0;
    }

    public void paint() {
        System.out.print("Круг с координатами: " + xCoordinate + ", " + yCoordinate);
        System.out.println(" с радиусом: " + radius1);
        System.out.println("-- Периметр круга равен " + this.getPerimeter());
    }
}
