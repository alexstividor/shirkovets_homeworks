package mypackage01;

public abstract class Figure {
    protected int xCoordinate;
    protected int yCoordinate;

    public Figure(int xCoordinate, int yCoordinate) {
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
    }

    public double getPerimeter() {
        return 0;
    }

    public void paint() {
        System.out.println("Фигура с координатами: " + xCoordinate + ", " + yCoordinate);
    }
}
