package mypackage01;

/*
Сделать класс Figure из задания 09 абстрактным.
Определить интерфейс, который позволит перемещать фигуру на заданные координаты.
Данный интерфейс должны реализовать только классы Circle и Square.
В Main создать массив "перемещаемых" фигур и сдвинуть все их в одну конкретную точку.
*/

public class Main {

    public static void main(String[] args) {
        // Создаем объекты классов с помощью конструкторов

        mypackage01.Square square = new mypackage01.Square(0, 23, 17);
        mypackage01.Circle circle = new mypackage01.Circle(4,12, 23.0);
        mypackage01.Square square1 = new mypackage01.Square(3, 11, 17);
        mypackage01.Circle circle1 = new mypackage01.Circle(7,5, 30.0);

        Teleport[] movingFigures = new Teleport[4];

        movingFigures[0] = square;
        movingFigures[1] = circle;
        movingFigures[2] = square1;
        movingFigures[3] = circle1;

        for (int i = 0; i < movingFigures.length; i++) {
            movingFigures[i].moveTo(2, 10);
        }

        square.paint();
        circle.paint();
        square1.paint();
        circle1.paint();
    }
}
