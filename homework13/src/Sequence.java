public class Sequence {
    // реализуем метод filter для фильтрации элементов оценивая их состояние
    public static int[] filter(int[] array, ByCondition condition) {
        // массив отфильтрованных элементов будет не меньше исходного
        // нужно узнать размер итогового массива
        int resultLength = 0;

        //при удовлетворении условия фильтра инкрементируем размер итогового массива
        for (int i = 0; i < array.length; i++) {
            if (condition.isOk(array[i])) {
                resultLength++;
            }
        }
        // создаем итоговый массив полученным размером
        int[] result = new int[resultLength];

        // присваиваем значения удовлетворяющие фильтру элементам итогового массива
        int resultPos = 0;
        for (int i = 0; i < array.length; i++) {
            if (condition.isOk(array[i])) {
                result[resultPos] = array[i];
                resultPos++;
            }
        }
        // возвращаем итоговый массив
        return result;
    }

    // создаем дополнительный статический метод для суммирования цифр числа
     static int countDigitsSum(int number){
        int sum = 0;
        for ( ; number > 0; number /= 10)
            sum += number % 10;
        return sum;
    }
}
