import java.sql.SQLOutput;

public class Main {
    /*Предусмотреть функциональный интерфейс
    interface ByCondition {
        boolean isOk(int number);
    }
    Реализовать в классе Sequence метод:
    public static int[] filter(int[] array, ByCondition condition) {
	...
    }
    Данный метод возвращает массив, который содержит элементы, удовлетворяющиие логическому выражению в condition.
    В main в качестве condition подставить:
            - проверку на четность элемента
            - проверку, является ли сумма цифр элемента четным числом.
*/

    public static void main(String[] args) {

        // создаем тестовый массив
        int[] array = {11, 162, 7821, 98, 101, 51, 32, 81, 0};

        // создаем массив отфильтрованных элементов на основе описанной в лямбда выражении реализации фунционального интерфейса
        // условие = элемент четный
        int[] a = Sequence.filter(array, number -> (number % 2) == 0 );
        // условие = сумма цифр элемента четная
        int[] b = Sequence.filter(array, number -> (Sequence.countDigitsSum(number)  % 2) == 0 );

        // выводим результаты в консоль
        System.out.print("Четные элементы из поданного на вход массива: ");
        for (int i : a) {
            System.out.print(i + "  ");
        }
        System.out.println();
        System.out.print("Элементы с четной суммой цифр из поданного на вход массива: ");
        for (int i : b) {
            System.out.print(i + "  ");
        }
    }

}
