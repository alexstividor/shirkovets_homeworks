import java.util.List;

/**
 * 21.11.2021
 * 20. Java IO
 *
 * @author Shirkovets Alexey

 */
public class Main {
    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("homework19/users.txt");
        List<User> users = usersRepository.findAll();

        for (User u : users) {
            System.out.println(u.getAge() + " " + u.getName() + " " + u.isWorker());
        }

        List<User> usersByAge = usersRepository.findByAge(25);
        List<User> workers = usersRepository.findByIsWorkerIsTrue();
        System.out.println("---------");

        for (User user : usersByAge) {
            System.out.println(user.getAge() + " " + user.getName() + " " + user.isWorker());
        }
        System.out.println("---------");
        for (User user : workers) {
            System.out.println(user.getAge() + " " + user.getName() + " " + user.isWorker());
        }

    }
}
