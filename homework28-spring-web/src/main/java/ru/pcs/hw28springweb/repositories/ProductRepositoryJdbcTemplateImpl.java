package ru.pcs.hw28springweb.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Controller;
import ru.pcs.hw28springweb.models.Product;


import javax.sql.DataSource;
import java.util.List;

@Controller
public class ProductRepositoryJdbcTemplateImpl implements ProductRepository {

    //language=SQL
    private static final String SQL_INSERT = "insert into product (name, price, quantity) values (?,?,?)";

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from product order by id";

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ProductRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final RowMapper<Product> userRowMapper = (row, rowNumber) -> {
        int productId = row.getInt("id");
        String productName = row.getString("name");
        Double productPrice = row.getDouble("price");
        int productCount = row.getInt("quantity");

        return new Product(productId, productName, productPrice, productCount);
    };

    @Override
    public List<Product> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, userRowMapper);
    }

    @Override
    public void save(Product product) {
        jdbcTemplate.update(SQL_INSERT, product.getProductName(), product.getProductPrice(), product.getProductCount());
    }

    //language=SQL
    private static final String SQL_SELECT_ALL_BY_PRICE = "select * from product where price = ?";

    //language=SQL
    private static final String SQL_SELECT_ALL_BY_ORDERS_COUNT =
            "select p.id, p.name, p.price, p.quantity " +
            "from order_list ol " +
            "left join product p on p.id =ol.product_id where ol.quantity = ?";

    @Override
    public List<Product> findAllByPrice(double price) {
        return jdbcTemplate.query(SQL_SELECT_ALL_BY_PRICE, userRowMapper, price);
    }

    @Override
    public List<Product> findAllByOrdersCount(int ordersCount) {
        return jdbcTemplate.query(SQL_SELECT_ALL_BY_ORDERS_COUNT, userRowMapper, ordersCount);
    }
}
