package ru.pcs.hw28springweb.repositories;

import ru.pcs.hw28springweb.models.Product;


import java.util.List;

public interface ProductRepository {
    List<Product> findAll();
    void save(Product product);
    List<Product> findAllByPrice(double price);
    List<Product> findAllByOrdersCount(int ordersCount);
}
