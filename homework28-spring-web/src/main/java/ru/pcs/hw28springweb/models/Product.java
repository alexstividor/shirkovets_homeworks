package ru.pcs.hw28springweb.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class Product {
    private Integer productId;
    private String productName;
    private double productPrice;
    private Integer productCount;
}
