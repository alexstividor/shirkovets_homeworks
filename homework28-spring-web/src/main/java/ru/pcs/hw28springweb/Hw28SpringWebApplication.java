package ru.pcs.hw28springweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Hw28SpringWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(Hw28SpringWebApplication.class, args);
    }

}
