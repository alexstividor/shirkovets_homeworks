package ru.pcs.hw28springweb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.pcs.hw28springweb.models.Product;
import ru.pcs.hw28springweb.repositories.ProductRepository;


@Controller
public class ProductController {
    @Autowired
    private ProductRepository productRepository;

    @PostMapping("/orders")
    public String addProduct(@RequestParam("productName") String productName,
                             @RequestParam ("productPrice") double productPrice,
                             @RequestParam ("productCount") Integer productCount) {

        Product product = Product.builder()
                .productName(productName)
                .productPrice(productPrice)
                .productCount(productCount)
                .build();

        productRepository.save(product);
        return "redirect:product_add.html";
    }
}
