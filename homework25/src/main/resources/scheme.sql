-- создаем таблицу Товар
create table product (
                         id serial primary key,
                         name varchar(30),
                         price decimal(8,2),
                         quantity integer check ( quantity >= 0 )
);

-- создаем таблицу Заказчик
create table buyer (
                       id serial primary key,
                       name varchar(30)
);

-- создаем таблицу Заказ
create table order_list (
                            product_id integer,
                            foreign key (product_id) references product (id),
                            buyer_id integer,
                            foreign key (buyer_id) references buyer (id),
                            date date,
                            quantity integer check ( quantity >= 0 )
);

insert into product(name, price, quantity)values ('Witcher 3', 1600.99, 3 );
insert into product(name, price, quantity)values ('Far Cry 6', 4490.00, 15 );
insert into product(name, price, quantity)values ('GTA V', 1999.95, 5 );
insert into product(name, price, quantity)values ('Red Dead Redemption 2', 3099.95, 3 );
insert into product(name, price, quantity)values ('EA FIFA 22', 4990.95, 14 );
insert into product(name, price, quantity)values ('One Of Us II', 2899.22, 17 );

insert into buyer(name) values ('H1ghSky1');
insert into buyer(name) values ('MrSavageM');
insert into buyer(name) values ('RealCrafty');
insert into buyer(name) values ('KP5ive');
insert into buyer(name) values ('TimTheFatman');
insert into buyer(name) values ('KingRichard3');
insert into buyer(name) values ('TrycepEric');
insert into buyer(name) values ('Ninja123');

insert into order_list (product_id, buyer_id, date, quantity) values (1, 2, TIMESTAMP '2021-11-19', 3);
insert into order_list (product_id, buyer_id, date, quantity) values (2, 4, TIMESTAMP '2021-11-25', 1);
insert into order_list (product_id, buyer_id, date, quantity) values (3, 5, TIMESTAMP '2021-11-28', 1);
insert into order_list (product_id, buyer_id, date, quantity) values (4, 6, TIMESTAMP '2021-12-01', 1);
insert into order_list (product_id, buyer_id, date, quantity) values (4, 7, TIMESTAMP '2021-12-02', 2);
insert into order_list (product_id, buyer_id, date, quantity) values (5, 1, TIMESTAMP '2021-12-03', 1);

-- внести в таблицу Товар введенные значения
insert into product (name, price, quantity) values (?,?,?);

-- выбрать все товары по id
select * from product order by id;

-- выбрать все товары по введенному значению цены
select * from product where price = ?;

-- найти все товары по количеству заказов, в которых они участвуют
select p.id, p.name, p.price, p.quantity
    from order_list ol
    left join product p on p.id =ol.product_id where ol.quantity = ?;
