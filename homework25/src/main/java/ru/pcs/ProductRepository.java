package ru.pcs;

import java.util.List;

/**
 * 15.11.2021
 * 20. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface ProductRepository {
    List<Product> findAll();
    void save(Product product);
    List<Product> findAllByPrice(double price);
    List<Product> findAllByOrdersCount(int ordersCount);
}
