package ru.pcs;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

public class Main {
    public static void main(String[] args) {
        DataSource dataSource = new DriverManagerDataSource(
                "jdbc:postgresql://localhost:5432/orders",
                "bob",
                "pas003dbs");
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        ProductRepository usersRepository = new ProductRepositoryJdbcTemplateImpl(dataSource);
        System.out.println(usersRepository.findAll().toString());
        System.out.println(usersRepository.findAllByPrice(1999.95));
        System.out.println(usersRepository.findAllByOrdersCount(1));

    }
}

