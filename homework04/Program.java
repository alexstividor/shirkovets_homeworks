import java.util.Scanner;

class Program { 
	public static void main(String[] args) {
		// создаем объект класса Scanner
		Scanner scanner = new Scanner(System.in);
		
		// Объявляем переменные
		// last, current, next (a-1,a,a+1) будут пробегать как окошко по водимым числам  	
    	int last, current, next;

    	// Счетчик локального минимума из a-1,a,a+1 инициализируем 0 
    	int localMinCount = 0;

    	// Принимаем первое число
    	int a = scanner.nextInt();

    	// Инициализируем введенным числом все переменные окна
    	last = current = next = a;

    	// Условие окончания считывания вводимых чисел
    	while (a != -1) {

    		// Основное условие локального минимума 
            if (current < last && next > current) {

            	// Инкременция счетчика при выполнении условия
                localMinCount++;
            }

            //считываем следующее число
        	a = scanner.nextInt();

        	// перемещаем окошко по вводимым числам
        	last = current;
        	current = next;
        	next = a;
    	}

    //выводим значение локального вывода в поток вывода
    System.out.println("Локальных минимумов - " + localMinCount);
	}
}
