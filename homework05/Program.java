import java.util.Scanner;

class Program05 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int a = scanner.nextInt();

		// минимум принимает значение наибольшей возможной цифры
		int minDigit = 9;

		// вводим переменную для перебора цифр
		int digit;

		// в цикле проверяем окончание ввода последовательности чисел
		while (a != -1) {

				//в цикле у введенного числа ищем каждую цифру
				while (a != 0) {
					digit = a % 10;

					//если очередная цифра числа меньше минимума, она становится минимумом
					if (minDigit > digit) {
						minDigit = digit;
					}
					a = a / 10;
				}
			// считываем очередное число 
			a = scanner.nextInt();
		}
		
		System.out.println("Меньшая цифра у всех введенных чисел - " + minDigit);
	}
}
