/*
На вход подается последовательность чисел, оканчивающихся на -1. 
Необходимо вывести число, которое присутствует в последовательности минимальное количество раз.
Гарантируется:
Все числа в диапазоне от -100 до 100.
Числа встречаются не более 2 147 483 647-раз каждое.
Сложность алгоритма - O(n)
*/


import java.util.Scanner;

class Program {

	// можно использовать массив из элементов типа int т.к. числа встречаются не более 2 147 483 647-раз каждое

	// создаем метод который выводит индекс первого встретившегося минимального ненулевого значения
	public static int MinAppearIndex (int[] array) {
		int valueOfMin = 2147483647;
		int indexOfMin = 0;
		for (int i = 0; i < array.length; i++) {
			if (array[i] != 0 & array[i] < valueOfMin) {
				valueOfMin = array[i];
				indexOfMin = i;
			}
		}
		return indexOfMin;
	}

	public static void main(String[] args) {
	
		Scanner inputInt = new Scanner(System.in);
	
		// cоздаем массив из n элементов для фиксации появления числа
		// из 100-(-100)+1 = 201 элемента 

		int[] arr = new int[201];
		
		// вводим число с потока ввода
	    int a = inputInt.nextInt();
	    int b ;
		while (a != -1) {


			//элемент массива будет инкрементироваться при появлени числа равного индексу элемента массива

			b = a+100;
			arr[b]++;
			a = inputInt.nextInt();
			}

		int result = MinAppearIndex(arr);
		System.out.println("ответ " + (result-100));


	}
}

		/*
		//для проверки - генерация случайных чисел
		int x = 0;
		int j = 0; 
		while (j < 10) {
			x = (int)(Math.random()*201);
			arr[x]+=1;
			j++;
		}

		// метод создания массива от minVal до maxVal
		public static int[] createArrayFromTo(int minVal, int maxVal) {
		int arrayLength = maxVal - minVal + 1;
		int[] array = new int[arrayLength];
		return array;
		}
		*/
		
