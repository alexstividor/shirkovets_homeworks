import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;


import static org.junit.jupiter.api.Assertions.*;

@DisplayName("class NumbersUtil is working when")
@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
class NumbersUtilTest {
    //тестируем класс NumbersUtil
    private final NumbersUtil numbersUtil = new NumbersUtil();

    @Nested
    @DisplayName("gcd() is working when")
    public class ForGcd {

        // тестируем на ноль в первом аргументе
        @ParameterizedTest(name = "throws IllegalArgumentException on zero ")
        @ValueSource(ints = {0})
        public void throws_exception_on_zero_in_first_argument(int number1) {
            assertThrows(IllegalArgumentException.class, () -> numbersUtil.gcd(number1, 1));
        }

        // тестируем на ноль во втором аргументе
        @ParameterizedTest(name = "throws IllegalArgumentException on zero")
        @ValueSource(ints = {0})
        public void throws_exception_on_zero_in_second_argument(int number1) {
            assertThrows(IllegalArgumentException.class, () -> numbersUtil.gcd(1, number1));
        }

        // тестируем на корректность на разных вариантах, включая отрицательные
        @ParameterizedTest(name = "return gcd({2}) of {0} and {1}")
        @CsvSource(value = {"-18, -12, 6", "9, 12, 3", "64, 48, 16", "-64, 48, 16", "1, 10, 1", "9, -12, 3"})
        public void return_correct_gcd(int a, int b, int result) {
            assertEquals(result, numbersUtil.gcd(a, b));
        }
    }
}