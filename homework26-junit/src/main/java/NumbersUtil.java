public class NumbersUtil {

    /**
     * возвращает наибольший общий делитель для любых двух целых чисел
     */
    public static int gcd(int a, int b) {
        if (a == 0 || b == 0) {
            throw new IllegalArgumentException();
        }

        int tmp;
        while (b != 0) {
            tmp = a % b;
            a = b;
            b = tmp;
        }
        return Math.abs(a);
    }
}
