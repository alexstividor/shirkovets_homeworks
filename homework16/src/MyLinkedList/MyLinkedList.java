package MyLinkedList;

/**
 * 11.11.2021
 * 17. LinkedList
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MyLinkedList<T> {

    private static class Node<T> {
        T value;
        Node<T> next;

        public Node(T value) {
            this.value = value;
        }
    }

    private Node<T> first;
    private Node<T> last;
    private int size;

    public void add(T element) {
        // создаю новый узел
        Node<T> newNode = new Node<>(element);
        if (size == 0) {
            first = newNode;
        } else {
            last.next = newNode;
        }
        last = newNode;
        size++;
    }

    public void addToBegin(T element) {
        Node<T> newNode = new Node<>(element);

        if (size == 0) {
            last = newNode;
        } else {
            newNode.next = first;
        }
        first = newNode;
        size++;
    }

    public int size() {
        return size;
    }

    /**
     * 13.11.2021
     * 16. LinkedList get() homework16.2
     *    Возвращает параметризованный элемент LinkedList по индексу от начала.
     *
     * @param index индекс узла, значение которого надо вернуть
     * @author Shirkovets Alexey
     */
    public T get(int index) {
        //
        Node<T> target = first;
        int counter = 0;
        while (target != null) {
            if (counter == index) {
                return target.value;
            } else {
                target = target.next;
                counter++;
            }
        }
        System.err.println("Index is out of range");
        return null;
    }
}
