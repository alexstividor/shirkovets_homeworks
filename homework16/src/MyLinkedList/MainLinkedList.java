package MyLinkedList;

public class MainLinkedList {
    public static void main(String[] args) {
        MyLinkedList<String> names = new MyLinkedList<String>();

        names.add("Alex");
        names.add("Marcell");
        names.add("Nikolay");
        names.add("Vasiliy");
        names.add("Dima");

        for (int i = 0; i <= 6; i++) {
            System.out.println(names.get(i));
        }
    }
}
