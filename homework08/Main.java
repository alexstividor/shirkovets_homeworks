class Main {
  public static void main(String[] args) {
	// создаем массив из 10 объектов класса Human
	Human[] humans = new Human[10];
	
	// присваиваем имена и веса объектам массива 
	for (int i = 0; i < humans.length; i++) {
		humans[i] = new Human();
		humans[i].setName("Player_" + i);
		humans[i].setWeight(rnd(60, 100));
	}
	// печатаем массив полей объектов массива
	print(humans);

	// делаем сортировку выбором объектов массива по полю вес
	for (int i = 0; i < humans.length; i++) {
		int min = humans[i].getWeight();
		int minIndex = i;
			for (int j = i + 1; j < humans.length; j++) {
                if (humans[j].getWeight() < min) {
                    min = humans[j].getWeight();
                    minIndex = j;
                }
            }

		// заводим объект класса временный для обмена значений
		// 
		Human temp = new Human();
		temp = humans[i];
        humans[i] = humans[minIndex];
        humans[minIndex] = temp;
	}

	System.out.println();
	print(humans);
  }
	// метод для выдачи случайного значения из диапазона
  	public static int rnd(int min, int max) {
	max -= min;
	return (int) (Math.random() * ++max) + min;
  }
	// метод для выдачи занчений полей объектов массива массива в поток вывода построчно
  	public static void print(Human[] humans) {
	  for (int i = 0; i < humans.length; i++) {
		System.out.println("Name: " + humans[i].getName() + " weight: " + humans[i].getWeight());
	 }
  }

}

/*На вход подается информация о людях в количестве 10 человек (имя - строка, вес - вещественное число).
Считать эти данные в массив объектов.
Вывести в отсортированном по возрастанию веса порядке.
*/