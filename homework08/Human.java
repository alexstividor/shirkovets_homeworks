public class Human {
	private String name;
	
	private int weight;

	public int getWeight() {
		return this.weight;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setWeight(int weight) {
		if (weight < 0 || weight > 250) {
			weight = 0;
		}
		this.weight = weight;
	}
}